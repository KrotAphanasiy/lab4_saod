#ifndef HASH_LIST
#define HASH_LIST

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <climits>

typedef struct list_record {
	char key[256];
	int value = 0;
	bool taken = false;
	int next_index = -1;
};

union Word {
	unsigned char word[2];
};

list_record* create_tab(int size);

unsigned short char_key_toNumeric(char* key);
unsigned int multiplicationHash(int k, unsigned int N);
unsigned int algWilliams(list_record* array, unsigned int mindex, unsigned int N);
void add_record(list_record* tab, char* key, int value, int size);
int search_record(list_record* tab, char* key, int size);


#endif
