#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "hash_list.h"


using namespace std;

int succeses = 0;
int fails = 0;
int search_tryes_succes = 0;
int search_tryes_fail = 0;
int search_tryes = 0;
double avg_succes = 0;
double avg_fail = 0;


int main() {
	int size = 1021;
	srand(4541);
	
	list_record* tab = create_tab(size);;

	char key[256];
	int value;

	FILE* in = fopen("input.txt", "r");
	FILE* for_search = fopen("for_search.txt", "r");
	FILE* debug = fopen("debug.txt", "w");

	
	for (int keys_am = 0; keys_am <= 800; keys_am++) {

			value = rand();
			fscanf(in, "%s", key);
			add_record(tab, key, value, 1021);
				
			if ((keys_am % 100 == 0) && (keys_am != 0)) {
				
				for (int i = 0; i < 500; i++) {
					fscanf(for_search, "%s", key);
					search_record(tab, key, 1021);
				}

				if (succeses)
					avg_succes = search_tryes_succes / succeses;
				else
					avg_succes = -1;
				if (fails)
					avg_fail = search_tryes_fail / fails;
				else
					avg_fail = -1;

				cout << keys_am << ": " << endl << "succeses " << succeses << "  fails " << fails << endl << "avg_succes " << avg_succes << "  avg_fails " << avg_fail << endl << endl;

				succeses = 0;
				fails = 0;
				avg_succes = 0;
				avg_fail = 0;
				rewind(for_search);
			}
	}
	delete tab;
	tab = NULL;

	fclose(in);
	in = NULL;

	fclose(for_search);
	for_search = NULL;

	return 0;
}
