#include "hash_list.h"


const unsigned long long int P = 65521;
extern int succeses;
extern int fails;
extern int search_tryes_succes;
extern int search_tryes_fail;
extern int search_tryes;
extern double avg_succes;
extern double avg_fail;


list_record* create_tab(int size) {
	size = size;
	list_record* tab_ptr = new list_record[size];
	return tab_ptr;
}


/////////////////////ПИЗДЕЦ/////////////////////////////////////////////////////////

unsigned short char_key_toNumeric(char* key) {
	unsigned short numeric_key = 0;

	Word word;
	word.word[0] = 0;
	word.word[1] = 0;

	int in_key_counter = 0;

	for (in_key_counter = 0; in_key_counter < strlen(key) - 1; in_key_counter++) {

		if (in_key_counter % 2 == 0) {

			word.word[0] = key[in_key_counter];

			if (numeric_key >= numeric_key + ((unsigned short)word.word[0] << 8)) {
				numeric_key = numeric_key + ((unsigned short)word.word[0] << 8) + 1;
			}
			else {
				numeric_key = numeric_key + ((unsigned short)word.word[0] << 8);
			}

		}
		else if (in_key_counter % 2 != 0) {

			word.word[1] = key[in_key_counter];

			if (numeric_key >= numeric_key + (unsigned short)word.word[1]) {
				numeric_key = numeric_key + (unsigned short)word.word[1] + 1;
			}
			else {
				numeric_key = numeric_key + (unsigned short)word.word[1];
			}

		}

	}

	return numeric_key;
}

////////////////////////////////////////////////////////////////////////////////////

unsigned int multiplicationHash(int k, unsigned int N)
{
	return ((P * k) % UINT_MAX) * N >> 32;
}

unsigned int algWilliams(list_record* array, unsigned int mindex, unsigned int N)
{
	static unsigned int R = N;
	if (array[mindex].taken)
	{
		R--;
		while (array[R].taken) {
			R--;
		}
		int i = mindex;
		while (array[i].next_index != -1) {
			i = array[i].next_index;

		}
		array[i].next_index = R;
		return R;
	}
	else
	{
		return mindex;
	}
}

void add_record(list_record* tab, char* key, int value, int size) {
	unsigned int index;
	index = algWilliams(tab, multiplicationHash(char_key_toNumeric(key), size), size);
	strcpy(tab[index].key, key);
	tab[index].value = value;
	tab[index].taken = true;
}

int search_record(list_record* tab, char* key, int size) {
	unsigned int index;
	index = multiplicationHash(char_key_toNumeric(key), size);

	bool found = false;

	while ((tab[index].next_index != -1) && !found) {
		search_tryes++;
		if (strcmp(tab[index].key, key) == 0)
		{
			found = true;
			succeses++;
			search_tryes_succes += search_tryes;
			search_tryes = 0;
			return (int)index;
		}
		index = tab[index].next_index;
	}
	
	if (!found) {
		fails++;
		search_tryes_fail += search_tryes;
		search_tryes = 0;
		return -1;
	}
}
